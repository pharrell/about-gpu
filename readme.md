# 关于gpu server

## 途径

两种途径来配置这个gpu server。

### 途径一：买零件自己组装

1. gpu数量：4～6， 

2. gpu型号：``rtx 2080ti``/``titan xp``， 其中2080ti价格区间是``10599～12999`` hkd。（这两种gpu型号都用的2018年nvidia新出的Turing architecture。titan xp 比2080ti多了一个GB的内存，贵了大概1000港币。）

3. cpu型号：intel i9-9900k，不推荐xeon。我们这个是gpu server， 所以重心应该放在gpu上：cpu其实不一定要xeon的，选择i9的都不错。
深水埗那边的电脑城都没有xeon零售，想要xeon的话需要跟店家预定；零售cpu最高型号是i9，价格差不多在**4350hkd**左右。
（cs depart里面有cpu cluster，我们自己这边也有个windows cpu server。）

4. Memory: 64GB (D4-3000LPX(BK) Vengeance Corsair), ``2640``hkd. （这个内存可以弄大一点, 比如128gb，要参考主板支持的slots数量。普通motherboard仅仅支持4个memory slots， each for 16GB， totally 64GB）

5. 主板：这个要购买支持4～6个gpu slots的主板，具体型号还需要查一下，我这边之前只了解过支持两个gpu slots的板子。对于超过两个gpu slots的板子深水埗那边要跟店家预定。

6. storage：这个10t ssd差不多在``1万``港币左右，10t hdd ``2398``hkd。

**Advantage**：相比较购买整机，至少可以多买一个gpu；如果cpu和ram那边可以压缩一点点预算，差不多可以多出来两个gpu。
**Disadvantage**：要自己花时间组装，不知道cs lab那边的staff可不可以帮忙？我们自己装照我这经验差不多要三天，包括装系统。如果cs lab有谁经验丰富的话，一天差不多了。


### 途径二：购买整机

购买整机的话配置没太多选择，

1. ``Reference #1``：https://lambdalabs.com/workstations/quad/customize

![lambda quad premium](quad.png)

2. ``Reference #2``：https://www.exxactcorp.com/Deep-Learning-NVIDIA-GPU-Solutions

![midrange](exact.png)

3. ``Reference #3``: 或者去深水埗电脑城那边对店家讲出我们要的配置，然后让他们报价，合适的话就可以预定。

**Advantage**：不需要自己装，很方便。
**Disadvantage**：感觉最多就是4个2080ti/titan xp 了，有一部分费用（简单算一下，差不多能有1～2万）会被抽走作为商家的装机利润。

## Info

[Computation Capability List for NVIDIA GPUs](https://developer.nvidia.com/cuda-gpus)。我们很大概率是要买2080ti或者titan xp的。其他的要么太贵，要么计算能力不够，内存太小.
 
## 我的一点想法

1. ``Ubuntu is a MUST``: os墙裂建议装UBUNTU server。因为ubuntu server好多东西可以用command line来完成，much more reliable and convenient than windows server，有的时候graphical user interface反而容易给系统增加额外的workload。另外deep learning community里面绝大部分都是用ubuntu的。
2. ``Queuing Mechanism matters``: 最好可以用HTCondor来manage job submission, providing queuing mechanism, 就像系里面的htcc和htgc cluster一样.